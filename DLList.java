/**
 * Doubly linked list
 *
 * This represent a doubly linked list with {@link DLNode} nodes.
 *
 * @author Sven Kuhlmann
 * @version 1.9
 */
public class DLList<T> {

	/** The node represent the head in the list */
	private DLNode<T> head;

	/** The node represent the last one in the list */
	private DLNode<T> last;

	/** The node represent amount of nodes linked in the list */
	private int amountOfNodes;

	/**
	 * This is the constructor. Clears the head and last node.
	 */
	public DLList() {
		this.amountOfNodes = 0;
		this.head = null;
		this.last = head;
	}

	/**
	 * Add new node to the list at a specific index.
	 *
	 * @param index Define where to add the item.
	 * @param data The data to add.
	 */
	public void add(int index, T data) {
		if (index <= amountOfNodes) {
			DLNode<T> before = this.getNode(index);
			DLNode<T> newNode = new DLNode<T>(data, null, null);
			if (amountOfNodes == 0) {
				this.head = newNode;
				this.last = this.head;
			} else {
				newNode.setNext(before);
				newNode.setPrevious(before.getPrevious());
				before.setPrevious(newNode);
				if (index == amountOfNodes - 1) {
					this.last.setNext(null);
					this.last = newNode;
				}
				if (index == 0) {
					newNode.setPrevious(null);
					this.head = newNode;
				} else {
					before.getPrevious().setNext(newNode);
				}
			}
			amountOfNodes++;
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	/**
	 * Add new node to the list at the end.
	 *
	 * @param data The data to add.
	 */
	public void add(T data) {
		DLNode<T> newNode = new DLNode<T>(data, null, null);
		if (amountOfNodes == 0) {
			this.head = newNode;
			this.last = this.head;
		} else {
			newNode.setPrevious(this.last);
			this.last.setNext(newNode);
			this.last = newNode;
		}
		amountOfNodes++;
	}

	/**
	 * Get data of a node.
	 *
	 * @param index The index of the node within the data to be returned.
	 * @return The data.
	 */
	public T get(int index) {
		return this.getNode(index).getData();
	}

	/**
	 * Helper to get an node at an index. Loop through nodes.
	 *
	 * @param index The index of node to be returned.
	 * @return The node.
	 */
	private DLNode<T> getNode(int index) {
		DLNode<T> current = null;
		if (index <= amountOfNodes) {
			if (index <= (amountOfNodes - 1) / 2) {
				// From first to index
				for (int i = 0; i <= index; i++) {
					if (i == 0) {
						current = this.head;
					} else {
						current = current.getNext();
					}
				}
			} else {
				// From last to index
				for (int i = amountOfNodes - 1; i >= index; i--) {
					if (i == amountOfNodes - 1) {
						current = this.last;
					} else {
						current = current.getPrevious();
					}
				}
			}
		} else {
			throw new IndexOutOfBoundsException();
		}
		return current;
	}

	/**
	 * Check if the list is empty.
	 *
	 * @return True/False if empty.
	 */
	public boolean isEmpty() {
		return amountOfNodes == 0;
	}

	/**
	 * Remove an node from the list.
	 *
	 * @param index The index of the node to remove.
	 * @return The removed node.
	 */
	public DLNode<T> remove(int index) {
		DLNode<T> oldNode = this.getNode(index);
		if (index <= amountOfNodes - 1) {
			if (index == 0) {
				this.head = oldNode.getNext();
			} else {
				DLNode<T> last = this.getNode(index - 1);
				if (index == amountOfNodes - 1) {
					last.setNext(null);
					this.last = last;
				} else {
					last.setNext(last.getNext().getNext());
				}
			}
			amountOfNodes--;
		} else {
			throw new IndexOutOfBoundsException();
		}
		return oldNode;
	}

}
