
/*
 * Test
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DLListTest {

	@Test
	public void testAdd() {
		DLList<String> list = new DLList<>();
		list.add("A");
		list.add("B");
		list.add("C");
		list.add("B");
		assertEquals("C", list.get(2));
	}

	@Test
	public void testAddAt() {
		Object[] a = { "E", "A", "B", "F", "C", "D" };
		DLList<String> list = new DLList<>();
		list.add("A");
		list.add("B");
		list.add("C");
		list.add("D");
		list.add(0, "E");
		list.add(3, "F");
		for (int i = 0; i <= a.length - 1; i++) {
			assertEquals(a[i], list.get(i));
		}
	}

	@Test
	public void testEmpty() {
		DLList<String> list = new DLList<>();
		assertTrue(list.isEmpty());
	}

	@Test
	public void testGet() {
		String values = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		Object[] a = new Object[values.length()];
		DLList<Object> list = new DLList<>();
		for (int i = 0; i <= values.length() - 1; i++) {
			char buffer = values.charAt(i);
			list.add(buffer);
			a[i] = buffer;
		}
		for (int i = 0; i < a.length; i++) {
			assertEquals(a[i], list.get(i));
		}
	}

}
