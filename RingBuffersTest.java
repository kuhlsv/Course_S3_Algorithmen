
/*
 * Test
 */
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RingBuffersTest {

	@Test
	public void testExpand() {
		int offset = 3;
		Object[] a = { "d", "e", null, "a", "b", "c" };
		Object[] b = RingBuffers.expand(a, offset);
		assertEquals(b.length, 2 * a.length);
		for (int i = 0; i < a.length; i++) {
			assertEquals(a[Math.floorMod(offset + i, a.length)], b[i]);
		}
	}

	@Test
	public void testShift() {
		int offset = 3;
		Object[] a = { "d", "e", null, "a", "b", "c" };
		Object[] b = a.clone();
		int i = 1;
		RingBuffers.shift(a, i, offset);
		for (int j = 0; j < i; j++) {
			assertEquals(b[Math.floorMod(offset + j, a.length)], a[Math.floorMod(offset + j, a.length)]);
		}
		for (int j = i; j < a.length - 1; j++) {
			System.out.println(b[Math.floorMod(offset + j, a.length)] + " " + a[Math.floorMod(offset + j + 1, a.length)]);
			assertEquals(b[Math.floorMod(offset + j, a.length)], a[Math.floorMod(offset + j + 1, a.length)]);
		}
	}

}
