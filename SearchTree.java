import java.util.ArrayList;

/*-	Structure		|
 *   			(Branch)
 * 			   /	|	\
 *	(Left Branch) (Data) (Right Branch)
 *		/  \					/  \
 */
/**
 * Search Tree
 *
 * This represent the tree with branches {@link SearchTreeBranch} and data-nodes
 * {@link SearchTreeNode}.
 *
 * @author Sven Kuhlmann
 * @version 1.7
 */
public class SearchTree<T> {

	/** This represent the root branch. */
	private SearchTreeBranch<T> rootBranch;

	/**
	 * This is the constructor.
	 */
	public SearchTree() {
		setRootBranch(null);
	}

	/**
	 * Get the root-branch of this tree.
	 *
	 * @return The root-branch.
	 */
	public SearchTreeBranch<T> getRootBranch() {
		return rootBranch;
	}

	/**
	 * Insert a value bind to a key. Use helper function
	 * {@link SearchTree#insertRecursion(SearchTreeBranch, Object, int)} for recursive call.
	 *
	 * @param key The key for the node.
	 * @param value The data for the key.
	 */
	public void insert(int key, T value) {
		this.setRootBranch(insertRecursion(this.rootBranch, value, key));
	}

	/**
	 * Helper function to insert data recusive by go down the branches. At first call give root
	 * branch.
	 *
	 * @param branch The branch to go down.
	 * @param key The key for the node.
	 * @param value The data for the key.
	 */
	private SearchTreeBranch<T> insertRecursion(SearchTreeBranch<T> branch, T value, int key) {
		SearchTreeBranch<T> currentBranch = branch;
		// Check branch else create new with data
		if (currentBranch == null) {
			branch = new SearchTreeBranch<T>(new SearchTreeNode<T>(key, value));
		} else {
			// Recursive check the left and right branch down
			if (key < currentBranch.getLeaf().getKey()) {
				currentBranch.setLeftBranch(insertRecursion(currentBranch.getLeftBranch(), value, key));
			} else if (key > branch.getLeaf().getKey()) {
				currentBranch.setRightBranch(insertRecursion(currentBranch.getRightBranch(), value, key));
			}
		}
		return branch;
	}

	/**
	 * Search recursive for a key in this root branch and return the data.
	 *
	 * @param key The key for the node.
	 * @return The data for the key.
	 */
	public T lookup(int key) {
		return this.lookup(key, this.rootBranch);
	}

	/**
	 * Search recursive for a key and return the data.
	 *
	 * @param key The key for the node.
	 * @param branch Specify a branch.
	 * @return The data for the key.
	 */
	public T lookup(int key, SearchTreeBranch<T> branch) {
		T result = null;
		if (branch != null) {
			// Check branch leaf for key
			if (branch.getLeaf().getKey() == key) {
				result = branch.getLeaf().getData();
			} else {
				// Check next left branch recursive
				result = lookup(key, branch.getLeftBranch());
				if (result == null) {
					// Check next right branch recursive
					result = lookup(key, branch.getRightBranch());
				}
			}
		}
		return result;
	}

	/**
	 * Set the root-branch of this tree.
	 *
	 * @param rootBranch The root-branch.
	 */
	public void setRootBranch(SearchTreeBranch<T> rootBranch) {
		this.rootBranch = rootBranch;
	}

	/**
	 * Convert the tree to a list. Values ordered by key. Calling recursive helper
	 * {@link SearchTree#toListRecursive(TreeNode)}
	 *
	 * @return The ArrayList of the tree.
	 */
	public ArrayList<T> toList() {
		ArrayList<T> BSTList = new ArrayList<T>();
		toListRecursive(this.rootBranch, BSTList);
		return BSTList;
	}

	/**
	 * Helper function to get the data recursive from the branches. At first call give root branch.
	 *
	 * @param branch The branch to go down.
	 */
	private void toListRecursive(SearchTreeBranch<T> branch, ArrayList<T> list) {
		if (branch != null) {
			toListRecursive(branch.getLeftBranch(), list);
			list.add(branch.getLeaf().getData());
			toListRecursive(branch.getRightBranch(), list);
		}
	}
}
