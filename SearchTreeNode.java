/**
 * Search Tree Node
 *
 * This represent the node with data of the search tree {@link SearchTree}.
 *
 * @author Sven Kuhlmann
 * @version 1.0
 */
public class SearchTreeNode<T> {

	/** This represent the key of the node. */
	private int key;

	/** This represent the data of the node. */
	private T data;

	/**
	 * This is the constructor with initial data and key.
	 */
	public SearchTreeNode(int key, T value) {
		this.setKey(key);
		this.setData(value);
	}

	/**
	 * Get the data of this node.
	 *
	 * @return The data.
	 */
	public T getData() {
		return data;
	}

	/**
	 * Get the key of this node.
	 *
	 * @return The key.
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Set the data of this node.
	 *
	 * @param value The data.
	 */
	public void setData(T value) {
		this.data = value;
	}

	/**
	 * Set the key of this node.
	 *
	 * @param key The key.
	 */
	public void setKey(int key) {
		this.key = key;
	}

}
