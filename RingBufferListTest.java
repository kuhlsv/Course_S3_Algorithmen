
/*
 * Test
 */
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RingBufferListTest {

	@Test
	public void testAdd() {
		RingBufferList<Object> a = new RingBufferList<>();
		Object[] b = { "d", "e", null, "a", "b", "c" };
		a.add("d");
		a.add("e");
		a.add(null);
		a.add("a");
		a.add("b");
		a.add("c");
		for (int i = 0; i < b.length; i++) {
			assertEquals(b[i], a.get(i));
		}
	}

	@Test
	public void testExpand() {
		String values = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		RingBufferList<Object> a = new RingBufferList<>();
		Object[] b = new Object[values.length()];
		for (int i = 0; i <= values.length() - 1; i++) {
			char buffer = values.charAt(i);
			b[i] = buffer;
			a.add(buffer);
		}
		for (int i = 0; i < b.length; i++) {
			assertEquals(b[i], a.get(i));
		}
	}

	@Test
	public void testShift() {
		RingBufferList<Object> a = new RingBufferList<>();
		Object[] b = { "d", "e", null, "m", "a", "b", "c" };
		a.add("e");
		a.add(null);
		a.add("a");
		a.add("b");
		a.add(0, "d");
		a.add(3, "m");
		a.add(a.size(), "c");
		for (int i = 0; i < b.length; i++) {
			assertEquals(b[i], a.get(i));
		}
	}
}
