/**
 * Search Tree Branch
 *
 * This represent the branch of the search tree {@link SearchTree}.
 *
 *
 * @author Sven Kuhlmann
 * @version 1.2
 */
public class SearchTreeBranch<T> {

	/** This represent the left next branch. */
	private SearchTreeBranch<T> leftBranch;

	/** This represent the right next branch. */
	private SearchTreeBranch<T> rightBranch;

	/** This represent a node with the data. */
	private SearchTreeNode<T> leaf;

	/**
	 * This is the constructor.
	 */
	public SearchTreeBranch() {
		setLeftBranch(null);
		setRightBranch(null);
	}

	/**
	 * This is the constructor with initial data-node.
	 */
	public SearchTreeBranch(SearchTreeNode<T> node) {
		this();
		setLeaf(node);
	}

	/**
	 * Get the data-node of this branch.
	 *
	 * @return The data-node.
	 */
	public SearchTreeNode<T> getLeaf() {
		return leaf;
	}

	/**
	 * Get the left next branch.
	 *
	 * @return The left branch.
	 */
	public SearchTreeBranch<T> getLeftBranch() {
		return leftBranch;
	}

	/**
	 * Get the right next branch.
	 *
	 * @return The right branch.
	 */
	public SearchTreeBranch<T> getRightBranch() {
		return rightBranch;
	}

	/**
	 * Set the data-node of this branch
	 *
	 * @param leaf The data-node.
	 */
	public void setLeaf(SearchTreeNode<T> leaf) {
		this.leaf = leaf;
	}

	/**
	 * Set the left next branch.
	 *
	 * @param leftBranch The left branch.
	 */
	public void setLeftBranch(SearchTreeBranch<T> leftBranch) {
		this.leftBranch = leftBranch;
	}

	/**
	 * Set the right next branch.
	 *
	 * @param rightBranch The right branch.
	 */
	public void setRightBranch(SearchTreeBranch<T> rightBranch) {
		this.rightBranch = rightBranch;
	}

}
