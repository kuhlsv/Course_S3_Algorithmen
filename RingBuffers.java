/**
 * Ringbuffer
 *
 * This has to functions to handle a ringbuffer array.
 *
 * @author Sven Kuhlmann
 * @version 1.1
 */
public class RingBuffers<T> {

	/**
	 * This method expand an ringbuffer arraylist by creating a new array and write all
	 * items starting at offset in the beginning of the new arraylist.
	 *
	 * @param arrayList The ringbuffer list to expand.
	 * @param offset The start of the ringbuffer list.
	 * @return The expanded new arraylist
	 */
	public static Object[] expand(Object[] arrayList, int offset) {
		Object[] newArrayList = new Object[arrayList.length * 2];
		int oldStart = offset;
		for (int i = 0; i < arrayList.length; i++) {
			if (oldStart + i <= arrayList.length - 1) {
				// Write all from offset to end in the new array
				newArrayList[i] = arrayList[oldStart + i];
			} else {
				// Write all from start to the offset in the new array
				newArrayList[i] = arrayList[i - offset];
			}
		}
		return newArrayList;
	}

	/**
	 * This method shift an ringbuffer arraylist at the index.
	 *
	 * @param arrayList The ringbuffer list to expand.
	 * @param index The index where to start shifting.
	 * @param offset The start of the ringbuffer list.
	 */
	public static void shift(Object[] arrayList, int index, int offset) {
		Object bufferPrevious = arrayList[offset + index + 1];
		// Shift all from start of the ringbuffer (offset + index)
		for (int j = offset + index; j < arrayList.length; j++) {
			Object bufferCurrent = arrayList[j];
			arrayList[j] = bufferPrevious;
			bufferPrevious = bufferCurrent;
		}
		// Shift all from the start of the array to the end of the ringbuffer
		for (int j = 0; j < offset; j++) {
			Object bufferCurrent = arrayList[j];
			arrayList[j] = bufferPrevious;
			bufferPrevious = bufferCurrent;
		}
	}
}
