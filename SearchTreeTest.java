
/*
 * Test
 */
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class SearchTreeTest {

	@Test
	public void testListAndInsert() {
		SearchTree<String> tree = new SearchTree<>();
		tree.insert(50, "1");
		tree.insert(150, "2");
		tree.insert(250, "3");
		tree.insert(30, "4");
		tree.insert(20, "5");
		tree.insert(1, "6");
		ArrayList<String> list = tree.toList();
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
		assertEquals(tree.getRootBranch().getLeaf().getData(), tree.getRootBranch().getLeaf().getData());
	}

	@Test
	public void testLookup() {
		SearchTree<String> tree = new SearchTree<>();
		tree.insert(50, "1");
		tree.insert(150, "2");
		tree.insert(250, "3");
		tree.insert(30, "4");
		tree.insert(20, "5");
		tree.insert(1, "6");
		assertEquals("2", tree.lookup(150));
	}

}
