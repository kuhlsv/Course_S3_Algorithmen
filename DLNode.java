/**
 * Doubly linked list node
 *
 * This represent a node with data for a doubly linked list.
 *
 * @author Sven Kuhlmann
 * @version 1.0
 */
public class DLNode<T> {

	/** The node represent the next in the list */
	private DLNode<T> nextNode;

	/** The node represent the previous in the list */
	private DLNode<T> previousNode;

	/** This contains the data for the node */
	private T data;

	/**
	 * This is the constructor with initial data, next and previous node.
	 */
	public DLNode(T value, DLNode<T> next, DLNode<T> previous) {
		this.setData(value);
		this.setNext(next);
		this.setPrevious(previous);
	}

	/**
	 * Get an the data of this node.
	 *
	 * @return The data.
	 */
	public T getData() {
		return this.data;
	}

	/**
	 * Get the following node.
	 *
	 * @return The next node.
	 */
	public DLNode<T> getNext() {
		return this.nextNode;
	}

	/**
	 * Get the previous node.
	 *
	 * @return The previous node.
	 */
	public DLNode<T> getPrevious() {
		return this.previousNode;
	}

	/**
	 * Set the data for this node.
	 *
	 * @param data The data
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * Set the following node.
	 *
	 * @param The next node.
	 */
	public void setNext(DLNode<T> next) {
		this.nextNode = next;
	}

	/**
	 * Set the previous node.
	 *
	 * @param The previous node.
	 */
	public void setPrevious(DLNode<T> previous) {
		this.previousNode = previous;
	}
}
