/**
 * Ringbuffer arraylist.
 *
 * This is a arraylist extended by the functions of a ringbuffer.
 *
 * @author Sven Kuhlmann
 * @version 1.3
 */
public class RingBufferList<T> {

	/** The integer represent the amount of items in the arraylist. */
	private int amountOfItems;

	/** The integer represent the starting offset of the ringbuffer arraylist */
	private int listOffset;

	/** The object represent the arraylist. */
	private Object[] list;

	/** The final integer define the size of a new arraylist. */
	private final int START_SIZE = 10;

	/**
	 * This is the constructor with {@link RingBuffers#START_SIZE}.
	 */
	public RingBufferList() {
		list = new Object[START_SIZE];
		this.amountOfItems = 0;
		this.listOffset = 0;
	}

	/**
	 * This is the constructor with initial start size.
	 */
	public RingBufferList(int size) {
		list = new Object[size];
		this.amountOfItems = 0;
		this.listOffset = 0;
	}

	/**
	 * Add a new Item to the arraylist at a specific index.
	 *
	 * @param index Define where to add the item.
	 * @param value The item to add.
	 */
	public void add(int index, T value) {
		// Expand list
		if (this.list.length == this.amountOfItems) {
			this.expand(this.listOffset);
		}
		// Shifting use TODO CHeck
		this.shift(index, this.listOffset);
		// Add item in front of offset
		if (this.listOffset + index <= this.list.length - 1) {
			this.list[this.listOffset + index] = value;
		} else {
			// Add behind offset
			this.list[(this.listOffset + index) - this.list.length - 1] = value;
		}
		this.amountOfItems++;
	}

	/**
	 * Add a new Item to the arraylist to the end.
	 *
	 * @param value The item to add.
	 */
	public void add(T value) {
		int lastItemPosition = this.listOffset + this.amountOfItems;
		int newItemPosition = lastItemPosition;
		// Expand list
		if (this.list.length == this.amountOfItems) {
			this.expand(this.listOffset);
		}
		// Add item before current offset int the ring
		if (newItemPosition > this.list.length - 1) {
			newItemPosition = (this.list.length - 1) - lastItemPosition;
		}
		this.list[newItemPosition] = value;
		this.amountOfItems++;
	}

	/**
	 * Calling {@link RingBuffers#expand(Object[], int)} from task 1.
	 *
	 * @param offset The start of the ringbuffer list.
	 */
	private void expand(int offset) {
		if (offset > this.list.length - 1) {
			throw new IndexOutOfBoundsException("Invalid index");
		} else {
			this.list = RingBuffers.expand(this.list, offset);
			// Reset offset
			this.listOffset = 0;
		}
	}

	/**
	 * Get an item.
	 *
	 * @param index The index of the item to be returned.
	 * @return The item.
	 */
	@SuppressWarnings("unchecked")
	public T get(int index) {
		if (index > this.list.length - 1) {
			throw new IndexOutOfBoundsException("Invalid index");
		} else {
			return (T) list[index];
		}
	}

	/**
	 * Check if the arraylist is empty.
	 *
	 * @return True/False if empty.
	 */
	public boolean isEmpty() {
		return amountOfItems == 0;
	}

	/**
	 * Calling {@link RingBuffers#shift(Object[], int, int)} from task 1.
	 *
	 * @param index The index where to start shifting.
	 * @param offset The start of the ringbuffer list.
	 */
	private void shift(int index, int offset) {
		if (index > this.list.length - 1 || offset > this.list.length - 1) {
			throw new IndexOutOfBoundsException("Invalid index");
		} else {
			RingBuffers.shift(this.list, index, offset);
		}
	}

	/**
	 * Get the size of the arraylist.
	 *
	 * @return The size.
	 */
	public int size() {
		return amountOfItems;
	}

}
