/**
 * Recursion
 *
 * This contains two functions to calculate the power.
 *
 * @author Sven Kuhlmann
 * @version 1.2
 */
public class Recursion {

	/**
	 * Function to calculate power.
	 *
	 * @param n The exponent.
	 * @param x The base.
	 */
	public static int power(int n, int x) {
		System.out.println("1");
		int result = 0;
		if (n == 0) {
			result = 1;
		} else if (n == 1) {
			result = x;
		} else {
			result = x * power(n - 1, x);
		}
		return result;
	}

	/**
	 * Better function to calculate power with x^n=(x^2)^n/2.
	 *
	 * @param n The exponent.
	 * @param x The base.
	 */
	public static int powerImproved(int n, int x) {
		System.out.println("2");
		int result = 0;
		if (n == 0) {
			result = 1;
		} else {
			int t = powerImproved(n / 2, x);
			if ((n % 2) == 0) {
				result = t * t;
			} else {
				result = x * t * t;
			}
		}
		return result;
	}
}
