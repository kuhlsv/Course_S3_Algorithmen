
/*
 * Test
 */
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RecursionTest {

	@Test
	public void testBothCrossed() {
		assertEquals(Recursion.power(3, 13), Recursion.powerImproved(3, 13));
	}

	@Test
	public void testImprovedSimple() {
		assertEquals(Recursion.power(9, 5), 1953125);
	}

	@Test
	public void testImprovedZero() {
		assertEquals(1, Recursion.powerImproved(0, 0));
	}

	@Test
	public void testSimple() {
		assertEquals(1953125, Recursion.powerImproved(9, 5));
	}

	@Test
	public void testZero() {
		assertEquals(Recursion.power(0, 0), 1);
	}

}
